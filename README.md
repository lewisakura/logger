@ayana/logger [![NPM](https://img.shields.io/npm/v/@ayana/logger.svg)](https://www.npmjs.com/package/@ayana/logger) [![Discord](https://discordapp.com/api/guilds/508903834853310474/embed.png)](https://discord.gg/eaa5pYf) [![Install size](https://packagephobia.now.sh/badge?p=@ayana/logger)](https://packagephobia.now.sh/result?p=@ayana/logger) [![XO code style](https://img.shields.io/badge/code_style-XO-5ed9c7.svg)](https://github.com/xojs/xo)
===

Useful and great looking logging made easy

What this is
---

This is a logging library inspired by the easy use of [slf4j](https://www.slf4j.org/).

How it helps with logging
---

Besides coloring your output, this logger makes it easy to log where you are logging from. This is done by some v8 StackTrace API magic. By that we can find out the module name and the path of your file in the module. This also works for modules in the `node_modules` folder (And probably also with yarn pnp). In addition the config is global, which makes it easy to control which modules and even single loggers are allowed to log at which levels.

Limitations
---

- There is currently no way to change the logging levels (This can be changed if needed)

Installation
---

With NPM

```
npm i @ayana/logger
```

With Yarn

```
yarn add @ayana/logger
```

Usage
---

This module works out of the box so you can start using it and worry about configuring it later.

Let's say your module is called `helloworld`, your JavaScript file is located in the folder `src/hello` and your main file is `src/index.js`:

```js
const Logger = require('@ayana/logger');

const log = Logger.get('Hello');

class Hello {
    constructor() {
        log.info('Hello World!');
        // Example output: 2018-07-16 18:00:00:000 INFO  [helloworld:hello.Hello]: Hello World!
    }
}

// You can also create a logger using a reference to the class. This will simply use the name of the class passed.
const logOfClass = Logger.get(Hello);
```

Package detection
---

The package detection finds your modules name and main file by it's package.json file. The shown path is relative to the main file. This means the name of every folder above your main file and the folder the main file is located in will never be shown in logs. The name of every folder underneath the one of your main file may be shown logs.

Restricting specific levels and loggers
---

Restrictions are made on the transport level so each transport can define what it wants to print out. For example you might want to log everything with your logging server but only shown Info messages and upwards in your console. Restrictions can be set when creating a transport or afterwards. In this example we showcase how you can edit the restrictions on the default transport:

```js
// The transports base level. This will be used if there's nothing specific defined in loggers. (Default: 'INFO')
Logger.getDefaultTransport().setLevel(LogLevel.INFO);

// Specifications for individual modules, packages and loggers.
Logger.getDefaultTransport().setLoggers([
    {
        name: 'helloworld:hello.Hello',
        level: 'DEBUG',
        // Whether the name should match exactly or if just the logger name starting with that name is enough (Optional, Default: false)
        exact: false,
    }
]);
```

Formatters
---

This module has a default formatter that can be customized in some ways. If that isn't enough you can extend the Formatter class and create your own formatter.

Currently, those are the options you can override in the default formatter:

| Field | Description |
|-|-|
| dateFormat | Moment.js compatible date format. Default: `YYYY-MM-DD HH:mm:ss:SSS` |
| colorErrors | Whether errors should be colored or not. Default: true |
| colorMeta | Whether line metadata (level and logger name) should be colored or not. Default: true |

```js
const { DefaultFormatter, Formatter, DefaultTransport } = require('@ayana/logger');

// Override default formatter
Logger.setFormatter(new DefaultFormatter({
    colorErrors: false,
    // ... further options go here
}));

// Create custom formatter
class MyFormatter extends Formatter {
    formatError(meta, error) {
        return error.toString();
    }

    formatMessage(meta, message) {
        return `${meta.level}: ${message}`
    }
}

// Use formatter with a transport
Logger.addTransport(new ConsoleTransport({
    formatter: new MyFormatter(),
}));

// When using another ConsoleTransport it makes sense to disable the default transport
Logger.disableDefaultTransport();
```

The meta object has the following properties:

| Field | Description |
|-|-|
| origin | The instance of the logger that initiated the current logging call |
| level | The log level of the message |
| uniqueMarker | The optional uniqueMarker used to denote multiple instance |
| input | The input message. This can either be a string or an Error |
| extra | An optional object of key value pairs with extra data (Can be used for a central logging system for example) |

Transports
---

Currently the only transport available is the ConsoleTransport, which is also the default. You can however implement your own transport and pass it to the library:

```js
const { Logger, Transport } = require('@ayana/logger');

class MyTransport extends Transport {
    print(meta, message) { // The meta passed here is the same as detailed above
        // Print it somewhere...
    }
}

Logger.addTransport(new MyTransport());

// If you want to disable the default transport you can do it like this
Logger.disableDefaultTransport();
```

The `uniqueMarker` argument is meant to be used to denote multiple instances of one class if needed. It is passed to the logger like this:

```js
log.info('message', 'uniqueMarker');
```

The `extra` argument is meant to be used for custom formatters, custom transports and attachment of further metadata. It is passed to the logger like this:

```js
log.info('message', null, { myField: true });
```

Links
---

[GitLab repository](https://gitlab.com/ayana/libs/logger)

[NPM package](https://npmjs.com/package/@ayana/logger)

License
---

Refer to the [LICENSE](LICENSE) file.
