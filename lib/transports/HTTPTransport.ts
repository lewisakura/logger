/*
 * Copyright 2018 - 2019 Ayana Developers <devs@ayana.io>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { LogMeta } from '../LogMeta';

import { Transport, TransportOptions } from './Transport';

/**
 * @ignore
 */
export interface HTTPBufferMessage {
	meta: LogMeta;
	message: string;
}

/**
 * @ignore
 */
export interface HTTPTransportOptions extends TransportOptions {
	endpoint: string;
	flushInterval?: number;
	onFlushError?: (code: number, reason?: string) => void;
}

/**
 * TODO: Unignore later
 *
 * @ignore
 */
export class HTTPTransport extends Transport<HTTPTransportOptions> {
	private readonly buffer: Array<HTTPBufferMessage>;
	private readonly lastFlush: number;

	public constructor(options: HTTPTransportOptions) {
		options = {
			endpoint: null,
			flushInterval: 0,
			...(options || {}),
		};
		super(options);

		if (this.options.endpoint == null) throw new Error('HTTPTransport requires a endpoint');

		this.buffer = [];
		this.lastFlush = null;

		// if we are bulking start a interval
		if (this.options.flushInterval > 0) {
			// consider tail calling from flush. instead of interval
			setInterval(() => {
				// TODO
			}, this.options.flushInterval);
		}
	}

	public prepareRequest(messages: Array<HTTPBufferMessage>) {
		return {
			body: { messages },
		};
	}

	public print(meta: LogMeta, message: string): void {
		this.buffer.push({ meta: meta, message });

		// bulking disabled, flush right away
		if (!this.options.flushInterval) this.flush();
	}

	private flush() {
		if (this.buffer.length < 1) {
			// if tail-calling, set a new timeout
			return;
		}

		// TODO: implement http & https clients

		// if tail cailing & bulking, setTimeout with this.options.flushInterval
	}
}
