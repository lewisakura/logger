/*
 * Copyright 2018 - 2019 Ayana Developers <devs@ayana.io>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Config } from '../config';
import { LogLevel, LogLevelValue } from '../constants';
import { Formatter } from '../formatter';
import { Logger } from '../Logger';
import { LogMeta } from '../LogMeta';

export interface TransportOptionsLogger {
	name: string;
	level: LogLevel;
	exact?: boolean;
}

export interface TransportOptions {
	formatter?: Formatter;
	level?: LogLevel;
	loggers?: Array<TransportOptionsLogger>;
}

export abstract class Transport<T extends TransportOptions> {
	private readonly knownLoggers: Map<Logger, LogLevelValue> = new Map();

	protected readonly options: T = {} as T;

	public constructor(options?: T) {
		this.options = { ...(options || {} as T) };

		this.setLevel(options.level);
		this.setFormatter(options.formatter);
		this.setLoggers(options.loggers);
	}

	public setLevel(level: LogLevel) {
		if (level == null) level = LogLevel.INFO;

		this.options.level = level;

		this.knownLoggers.clear();
	}

	public setFormatter(formatter: Formatter) {
		// Reset to default formatter if null or undefined is given
		if (formatter == null) {
			delete this.options.formatter;

			return;
		}

		// Check instance
		if (!(formatter instanceof Formatter)) throw new Error('Invalid formatter');

		this.options.formatter = formatter;
	}

	public setLoggers(loggers: Array<TransportOptionsLogger>) {
		// Deep clone loggers so no further changes can be made
		if (Array.isArray(loggers)) {
			loggers = JSON.parse(JSON.stringify(loggers));
		} else {
			loggers = [];
		}

		this.options.loggers = loggers;

		this.knownLoggers.clear();
	}

	private getAllowedLevel(logger: Logger): LogLevelValue {
		if (this.knownLoggers.has(logger)) return this.knownLoggers.get(logger);

		let allowLevel = LogLevelValue[this.options.level];

		const identifier = `${logger.packageName}:${logger.packagePath}${logger.name}`;
		for (const log of this.options.loggers) {
			if (log.exact && identifier !== log.name) continue;
			if (!log.exact && !identifier.startsWith(log.name)) continue;

			if (log.level === LogLevel.OFF) {
				allowLevel = LogLevelValue.OFF;
				break;
			}

			allowLevel = LogLevelValue[log.level];
			break;
		}

		this.knownLoggers.set(logger, allowLevel);

		return allowLevel;
	}

	// tslint:disable-next-line:max-params
	public log(origin: Logger, level: LogLevel, input: string | Error | (() => string | Error), uniqueMarker?: string, extra?: any): LogMeta {
		const allowLevel = this.getAllowedLevel(origin);

		if (allowLevel === LogLevelValue.OFF || level === LogLevel.OFF) return null;
		if (LogLevelValue[level] > allowLevel) return null;

		if (typeof input === 'function') input = input();

		const meta: LogMeta = {
			origin,
			level,
			uniqueMarker,
			input,
			extra,
		};

		this.formatAndPrint(meta);

		return meta;
	}

	public logMeta(meta: LogMeta) {
		const allowLevel = this.getAllowedLevel(meta.origin);
		if (allowLevel === LogLevelValue.OFF || meta.level === LogLevel.OFF) return;

		this.formatAndPrint(meta);
	}

	private formatAndPrint(meta: LogMeta) {
		let message: string;
		if (this.options.formatter != null) {
			message = this.options.formatter.format(meta);
		} else {
			message = Config.getInstance().formatter.format(meta);
		}

		this.print(meta, message);
	}

	protected abstract print(meta: LogMeta, message: string): void;
}
