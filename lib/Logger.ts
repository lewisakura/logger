/*
 * Copyright 2018 - 2019 Ayana Developers <devs@ayana.io>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import * as path from 'path';

import { Config } from './config';
import { LogLevel } from './constants';
import { DefaultFormatter, Formatter } from './formatter';
import { LogMeta } from './LogMeta';
import { Transport } from './transports';
import { PackageDetector } from './util';

/**
 * Logger main class. Use Logger.get() to create a new Logger.
 */
export class Logger {
	private static readonly detector: PackageDetector = new PackageDetector();

	/**
	 * The name of the current logger. This is derived from the argument in Logger.get().
	 */
	public readonly name: string;
	/**
	 * The package name of the current logger. This is derived from the package.json file of the caller project.
	 */
	public readonly packageName: string;
	/**
	 * The package path of the current logger. This is derived from the location Logger.get() was ran in.
	 */
	public readonly packagePath: string;
	/**
	 * Extra data for this logger. This will be used additionally to the extra data passed in every log call.
	 */
	public readonly extra: { [key: string]: string };

	private constructor(name: string | (() => string), pkgName: string | (() => string), pkgPath: string | (() => string), extra: { [key: string]: string }) {
		if (typeof name === 'function') {
			Object.defineProperty(this, 'name', {
				get: name,
				configurable: false,
				enumerable: false,
			});
		} else {
			Object.defineProperty(this, 'name', {
				value: name,
				writable: false,
				configurable: false,
				enumerable: true,
			});
		}

		if (typeof pkgName === 'function') {
			Object.defineProperty(this, 'packageName', {
				get: pkgName,
				configurable: false,
				enumerable: false,
			});
		} else {
			Object.defineProperty(this, 'packageName', {
				value: pkgName,
				writable: false,
				configurable: false,
				enumerable: true,
			});
		}

		if (typeof pkgPath === 'function') {
			Object.defineProperty(this, 'packagePath', {
				get: pkgPath,
				configurable: false,
				enumerable: false,
			});
		} else {
			Object.defineProperty(this, 'packagePath', {
				value: pkgPath,
				writable: false,
				configurable: false,
				enumerable: true,
			});
		}

		Object.defineProperty(this, 'extra', {
			value: extra,
			writable: false,
			configurable: false,
			enumerable: false,
		});
	}

	/**
	 * Logs a message with [[LogLevel]] *ERROR*
	 *
	 * @param log The string or error that should be logged. This can also be a function returning a string or an error. The function will only be called if the result is acutally logged
	 * @param uniqueMarker Optional. The unique marker for denoting different instances of a class
	 * @param extra Optional. An object containing additional data that can be used later on
	 *
	 * @see Logger#log
	 */
	public error(log: string | Error | (() => string | Error), uniqueMarker?: string, extra?: { [key: string]: any }) {
		this.log(LogLevel.ERROR, log, uniqueMarker, extra);
	}

	/**
	 * Logs a message with [[LogLevel]] *WARN*
	 *
	 * @param log The string or error that should be logged. This can also be a function returning a string or an error. The function will only be called if the result is acutally logged
	 * @param uniqueMarker Optional. The unique marker for denoting different instances of a class
	 * @param extra Optional. An object containing additional data that can be used later on
	 *
	 * @see Logger#log
	 */
	public warn(log: string | Error | (() => string | Error), uniqueMarker?: string, extra?: { [key: string]: any }) {
		this.log(LogLevel.WARN, log, uniqueMarker, extra);
	}

	/**
	 * Logs a message with [[LogLevel]] *INFO*
	 *
	 * @param log The string or error that should be logged. This can also be a function returning a string or an error. The function will only be called if the result is acutally logged
	 * @param uniqueMarker Optional. The unique marker for denoting different instances of a class
	 * @param extra Optional. An object containing additional data that can be used later on
	 *
	 * @see Logger#log
	 */
	public info(log: string | Error | (() => string | Error), uniqueMarker?: string, extra?: { [key: string]: any }) {
		this.log(LogLevel.INFO, log, uniqueMarker, extra);
	}

	/**
	 * Logs a message with [[LogLevel]] *DEBUG*
	 *
	 * @param log The string or error that should be logged. This can also be a function returning a string or an error. The function will only be called if the result is acutally logged
	 * @param uniqueMarker Optional. The unique marker for denoting different instances of a class
	 * @param extra Optional. An object containing additional data that can be used later on
	 *
	 * @see Logger#log
	 */
	public debug(log: string | Error | (() => string | Error), uniqueMarker?: string, extra?: { [key: string]: any }) {
		this.log(LogLevel.DEBUG, log, uniqueMarker, extra);
	}

	/**
	 * Logs a message with [[LogLevel]] *TRACE*
	 *
	 * @param log The string or error that should be logged. This can also be a function returning a string or an error. The function will only be called if the result is acutally logged
	 * @param uniqueMarker Optional. The unique marker for denoting different instances of a class
	 * @param extra Optional. An object containing additional data that can be used later on
	 *
	 * @see Logger#log
	 */
	public trace(log: string | Error | (() => string | Error), uniqueMarker?: string, extra?: { [key: string]: any }) {
		this.log(LogLevel.TRACE, log, uniqueMarker, extra);
	}

	/**
	 * Logs a message.
	 *
	 * @param level The log level
	 * @param log The string or error that should be logged. This can also be a function returning a string or an error. The function will only be called if the result is acutally logged
	 * @param uniqueMarker Optional. The unique marker for denoting different instances of a class
	 * @param extra Optional. An object containing additional data that can be used later on
	 */
	public log(level: LogLevel, log: string | Error | (() => string | Error), uniqueMarker?: string, extra?: { [key: string]: any }) {
		let meta: LogMeta = null;
		for (const transport of Config.getInstance().transports) {
			if (meta == null) meta = transport.log(this, level, log, uniqueMarker, { ...this.extra, ...extra });
			else transport.logMeta(meta);
		}
	}

	/**
	 * Creates a new logger instance for the current context.
	 *
	 * @param forClass A string or a Class for the loggers name
	 * @param extra Optional. An object containing additional data that will be appended on every log call
	 *
	 * @returns A new logger instance
	 */
	public static get(forClass?: string | Function, extra?: { [key: string]: any }) {
		let name;

		if (forClass == null || forClass === '') name = 'index.js';
		else if (typeof forClass === 'function') name = forClass.name;
		else if (typeof forClass === 'string') name = forClass;
		else throw new Error('Logger.get(): Invalid forClass parameter. Use a string or a named function');

		const callerDirectory = Logger.detector.getCallerDirectory();
		const projectRoot = Logger.detector.getRootOf(callerDirectory);
		const pkg = Logger.detector.getInfo(projectRoot);

		let pkgMain = '';
		if (pkg.loggerBase != null) pkgMain = pkg.loggerBase;
		else if (pkg.main != null) pkgMain = path.dirname(pkg.main);

		// Project file root. Used as a base to find the package path
		const pkgBase = path.join(projectRoot, pkgMain);

		// Relative path that gets printed in the end
		let pkgPath = path.relative(pkgBase, callerDirectory);

		// Check path starting with . or .. which means it is not a child of the pkgBase directory
		if (pkgPath.startsWith('.')) throw new Error('Logger.get(): This file is not in the logger base path for this project');

		pkgPath = pkgPath.replace(path.sep, '.');

		if (pkgPath.length > 0 && name !== 'index.js') pkgPath = `${pkgPath}.`;
		if (name === 'index.js') name = '';

		return new Logger(name, pkg.name, pkgPath, extra);
	}

	/**
	 * Creates a new custom logger instance without running any package detection.
	 * The first three arguments can also be functions returning the specified value so they can be changed.
	 * The functions will be called for every log-call.
	 *
	 * @param name The loggers name
	 * @param packageName The loggers package name
	 * @param packagePath The loggers package path (This should to end with a "." if it's not empty so it looks correct)
	 * @param extra Optional. An object containing additional data that will be appended on every log call
	 *
	 * @returns A new logger instance
	 */
	public static custom(name: string | (() => string), packageName: string | (() => string), packagePath: string | (() => string), extra?: { [key: string]: any }) {
		return new Logger(name, packageName, packagePath, extra);
	}

	/**
	 * Changes the global formatter.
	 * Note that this will affect every transport not explicitly defining their own formatter.
	 * If you want to only change the formatter on the default transport use `Logger.getDefaultTransport().setFormatter()`.
	 * The formatter must extend the class [[Formatter]].
	 * Alternatively `null` can be passed to reset the global formatter to the [[DefaultFormatter]].
	 *
	 * @param formatter The new global formatter
	 */
	public static setFormatter(formatter: Formatter) {
		// Reset to default formatter if null or undefined is given
		if (formatter == null) formatter = new DefaultFormatter();

		// Check instance
		if (!(formatter instanceof Formatter)) throw new Error('Invalid formatter');

		Config.getInstance().formatter = formatter;
	}

	/**
	 * Adds a new transport.
	 * Note that the transport will get all logging data from every installed module that uses this logging library.
	 * The transport must extend the class [[Transport]].
	 *
	 * @param transport The transport to be added
	 */
	public static addTransport(transport: Transport<any>) {
		if (transport == null) return;

		if (!(transport instanceof Transport)) throw new Error('Invalid transport');

		Config.getInstance().transports.push(transport);
	}

	/**
	 * Returns the default [[ConsoleTransport]] or null if the default transport has been disabled.
	 *
	 * @returns The default [[ConsoleTransport]]
	 */
	public static getDefaultTransport() {
		return Config.getInstance().defaultTransport;
	}

	/**
	 * Disables the default [[ConsoleTransport]].
	 * Note that calling this will affect every installed module that uses this logging library.
	 */
	public static disableDefaultTransport() {
		Config.getInstance().disableDefaultTransport();
	}

	/**
	 * Returns the version major of this package.
	 * This is used by @ayana/logger-api to determine compatibility.
	 *
	 * @returns The version major of this package.
	 */
	public static getVersionMajor(): number {
		return Number(Config.getInstance().major);
	}
}
