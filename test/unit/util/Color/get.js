'use strict';

const { Color } = require('../../../../build/util/Color');

describe('#get', function() {
	const getClean = () => {
		const tested = new Color();

		return tested;
	};

	it('should return a colored string if enabled', function() {
		const tested = getClean();

		expect(
			tested.get('red', 'MockString'),
			'not to be',
			'MockString'
		);
	});

	it('should return an uncolored string if disabled', function() {
		const tested = getClean();

		tested.disabled = true;

		expect(
			tested.get('red', 'MockString'),
			'to be',
			'MockString'
		);
	});
});
