'use strict';

const { PackageDetector } = require('../../../../build/util/PackageDetector');

describe('#getCallerDirectory', function() {
	it('should return the callers directory', function() {
		const tested = new PackageDetector();

		const expectedPath = '/home/ayana/Workspaces/ayana/logger';
		const fileNameFake = sinon.fake.returns(`${expectedPath}/index.js`);

		tested.getCallStack = sinon.fake.returns([
			'Error line',
			'Call to getCallStack()',
			'Call to getCallerDirectory()',
			{
				getFileName: fileNameFake,
			},
		]);

		const dir = tested.getCallerDirectory();

		expect(dir, 'to be', expectedPath);
		sinon.assert.calledOnce(tested.getCallStack);
		sinon.assert.calledOnce(fileNameFake);
	});
});
